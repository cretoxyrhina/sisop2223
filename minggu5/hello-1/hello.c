#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>

int hello_init(void)
{
	printk(KERN_INFO "Halo dunia, saya lapar\n");
	return 0; //Non-zero return means that the module couldn't be loaded.
}

void hello_cleanup(void)
{
	printk(KERN_INFO "Bersihkan modul kernel bersihkan hati\n");
}

module_init(hello_init);
module_exit(hello_cleanup);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Demo untuk kelas Sisop 19 Maret 2024");
MODULE_AUTHOR("FRW");
